#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
	mkfifo("/tmp/loggerfifo",0777);
	int fd = open("/tmp/loggerfifo",O_RDONLY);
	
	while(1)
	{
		char buffer[200];
		read(fd,buffer,sizeof(buffer));
		printf("%s\n", buffer);
	}
	close(fd);
	unlink("/tmp/loggerfifo");
	return 0;
}
